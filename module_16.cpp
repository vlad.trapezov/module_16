﻿#include <iostream>
#include <time.h>

using std::cout;

int main()
{
    struct tm buf;

    time_t t = time(NULL);
    localtime_s(&buf, &t);

    const int Size = 10;

    int array[Size][Size];
    
    int D = buf.tm_mday % Size;

    int Sum = 0, Line = 0;
   
    for (int i = 0; i < Size; i++)
    {
        cout << "Line_" << i << ":  ";

        for (int j = 0; j < Size; j++)
        {
            array[i][j] = i + j;

            cout << array[i][j] << ' ';

            if (D == i)
            {
                Line = i;

                Sum += array[i][j];
            }
        }
        cout << "\n";
    }

 
    cout << "Today is " << buf.tm_mday << "\n";
    cout << "Mday % N = " << D << "\n";
    cout << "Sum of line: " << Sum << "\n";

    return 0;

}




